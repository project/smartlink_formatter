# SmartLink Formatter

The SmartLink Formatter module offers a link formatter that transforms 
the output of a link field into various attributes within the anchor 
element.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/smartlink_formatter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/smartlink_formatter).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to selected entity view mode and select "Smart Link.


## Maintainers

- MS Drupal-Xpert - [manpreetsingh154](https://www.drupal.org/u/manpreetsingh154)
