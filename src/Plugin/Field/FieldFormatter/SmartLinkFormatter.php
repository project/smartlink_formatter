<?php

namespace Drupal\smartlink_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\link\LinkItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Smart Link' formatter.
 *
 * @FieldFormatter(
 *   id = "smartlink_formatter",
 *   label = @Translation("Smart Link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class SmartLinkFormatter extends FormatterBase {

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('path.validator'),
      $container->get('token')
    );
  }

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a new \Drupal\smartlink_formatter\Plugin\Field\FieldFormatte\SmartLinkFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator service.
   * @param \Drupal\Core\Utility\Token $tokenService
   *   The token service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PathValidatorInterface $path_validator, Token $token_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings,$path_validator);
    $this->pathValidator = $path_validator;
    $this->tokenService = $token_service;
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'trim_length' => '80',
      'url_only' => '',
      'url_plain' => '',
      'rel' => '',
      'noopener' => '',
      'noreferrer' => '',
      'link_class' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['url_only']['#access'] = $this->getPluginId() == 'smartlink_formatter';
    $elements['url_plain']['#access'] = $this->getPluginId() == 'smartlink_formatter';
    $elements['link_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#default_value' => $this->getSetting('link_class'),
      '#required' => FALSE,
      '#states' => [
        'invisible' => [
          ':input[name*="url_plain"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['rel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to links'),
      '#return_value' => 'nofollow',
      '#default_value' => $this->getSetting('rel'),
      '#states' => [
        'invisible' => [
          ':input[name*="url_plain"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['noopener'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noopener" to links'),
      '#return_value' => 'noopener',
      '#default_value' => $this->getSetting('noopener'),
      '#states' => [
        'invisible' => [
          ':input[name*="url_plain"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['noreferrer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noreferrer" to links'),
      '#return_value' => 'noreferrer',
      '#default_value' => $this->getSetting('noreferrer'),
      '#states' => [
        'invisible' => [
          ':input[name*="url_plain"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['target'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new window'),
      '#return_value' => '_blank',
      '#default_value' => $this->getSetting('target'),
      '#states' => [
        'invisible' => [
          ':input[name*="url_plain"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    if (!empty($settings['trim_length'])) {
      $summary[] = $this->t('Link text trimmed to @limit characters', ['@limit' => $settings['trim_length']]);
    }
    else {
      $summary[] = $this->t('Link text not trimmed');
    }
    if ($this->getPluginId() == 'smartlink_formatter' && !empty($settings['url_only'])) {
      if (!empty($settings['url_plain'])) {
        $summary[] = $this->t('Show URL only as plain text');
      }
      else {
        $summary[] = $this->t('Show URL only');
      }
    }
    if(empty($settings['url_plain'])){
      if (!empty($settings['rel'])) {
        $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel']]);
      }
      if (!empty($settings['noopener'])) {
        $summary[] = $this->t('Add rel="@noopener"', ['@noopener' => $settings['noopener']]);
      }
      if (!empty($settings['noreferrer'])) {
        $summary[] = $this->t('Add rel="@noreferrer"', ['@noreferrer' => $settings['noreferrer']]);
      }
      if (!empty($settings['link_class'])) {
        $summary[] = $this->t('Add class (@link_class) to the link', ['@link_class' => $settings['link_class']]);
      }
      if (!empty($settings['target'])) {
        $summary[] = $this->t('Open link in new window');
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $url = $this->buildUrl($item);
      $link_title = $url->toString();

      // If the title field value is available, use it for the link text.
      if (empty($settings['url_only']) && !empty($item->title)) {
        // Unsanitized token replacement here because the entire link title
        // gets auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $link_title =  $this->tokenService->replace($item->title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);
      }

      // Trim the link text to the desired length.
      if (!empty($settings['trim_length'])) {
        $link_title = Unicode::truncate($link_title, $settings['trim_length'], FALSE, TRUE);
      }

      if (!empty($settings['url_only']) && !empty($settings['url_plain'])) {
        $element[$delta] = [
          '#plain_text' => $link_title,
        ];
      }
      else {
        $element[$delta] = [
          '#type' => 'link',
          '#title' => $link_title,
          '#options' => $url->getOptions(),
        ];
        $element[$delta]['#url'] = $url;

        if (!empty($item->_attributes)) {
          $element[$delta]['#options'] += ['attributes' => []];
          $element[$delta]['#options']['attributes'] += $item->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and should not be rendered in the field template.
          unset($item->_attributes);
        }
      }
    }

    return $element;
  }

  /**
   * Builds the \Drupal\Core\Url object for a link field item.
   *
   * @param \Drupal\link\LinkItemInterface $item
   *   The link field item being rendered.
   *
   * @return \Drupal\Core\Url
   *   A Url object.
   */
  protected function buildUrl(LinkItemInterface $item) {
    $url = $item->getUrl() ?: Url::fromRoute('<none>');

    $settings = $this->getSettings();
    $options = $item->options;
    $options += $url->getOptions();
    $relArray = [];
    // Add optional 'rel' attribute to link options.
    if (!empty($settings['rel'])) {
      $relArray[] = $settings['rel'];
    }
    if (!empty($settings['noopener'])) {
      $relArray[] = $settings['noopener'];
    }
    if (!empty($settings['noreferrer'])) {
      $relArray[] = $settings['noreferrer'];
    }
    $options['attributes']['rel'] = implode(' ', $relArray);
    // Add optional 'target' attribute to link options.
    if (!empty($settings['target'])) {
      $options['attributes']['target'] = $settings['target'];
    }
    if (!empty($settings['link_class'])) {
      $options['attributes']['class'][] = $settings['link_class'];
    }
    $url->setOptions($options);

    return $url;
  }

}
